let person1 = {
    firstName: "Hemi", 
    lastName: "Turei",
    age: 19,
    study: "Part Time"
}

let person2 = {
    firstName: "Saanvi", 
    lastName: "Patel",
    age: 22,
    study: "Full Time"
}

let string1 = "Property values of 'person 1': \n"
let string2 = "Property values of 'person 2': \n"

for(let x in person1) {
    string1 += person1[x] + "\n"
}

for(let x in person2) {
    string2 += person2[x] + "\n"
}

console.log(string1)
console.log(string2)

fullName1 = person1.firstName + " " + person1.lastName
fullName2 = person2.firstName + " " + person2.lastName




console.log("")

console.log(`'Person 1' Full Name: ${fullName1}`)
console.log(`'Person 2' Full Name: ${fullName2}`)

person1.age = 21
person2.age = 24

console.log("")

console.log("'Person1' with new age:")
console.log(person1)

console.log("'Person2' with new age:")
console.log(person2)

person1.nationality = "Kiwi"
person2.nationality = "Indian"

console.log("Properties of 'person 1' and 'person2' (with 'nationality')")
console.log(person1)
console.log(person2)

delete person1.study
delete person2.study

console.log("\nProperties of 'person1' and person2 ('study' deleted):")            
console.log(person1)
console.log(person2)

console.log("")