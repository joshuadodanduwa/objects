let car1 = {year: "2001 ", make: "Honda ", model: "Stream ", price: "$3000 "}

let car2 = {
    year:   "1997 ",
    make:   "Lexus ",
    model:  "ES300 ",
    price:  "$5000 "
}

let car3 = new Object()
car3.year = "2017 ";
car3.make = "Chevrolet ";
car3.model = "Camaro ";
car3.price = "$50000 ";

let string1 = "--- Car 1 ---\n"
let string2 = "--- Car 2 ---\n"
let string3 = "--- Car 3 ---\n"

for(let x in car1)
{
    string1 += car1[x] + "\n"
}

for(let x in car2)
{
    string2 += car2[x] + "\n"
}

for(let x in car3)
{
    string3 += car3[x] + "\n"
}

console.log(string1)
console.log(string2)
console.log(string3)